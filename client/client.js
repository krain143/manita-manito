Meteor.startup(function () {
    
});

Template.groupList.helpers({
    groups: function() {
        return Groups.find({});
    },

});

Template.group.helpers({
    isRegistered: function() {
        if (!Meteor.user()) {
            return false;
        }
        var gmm = ManitoManita.find({
            group_id: this._id,
            user_id: Meteor.user()._id
        });
        var isRegistered = gmm.count() > 0;

        return isRegistered;
    }
});

Template.group.events({
    'click .register': function(event) {
        window.location = '/register/'+this._id;
    },
    'click .unregister': function(event) {
        var registeredCodeName = ManitoManita.findOne({
            group_id: this._id,
            user_id: Meteor.user()._id
        });

        ManitoManita.remove(registeredCodeName._id);
    }
});

Template.register_form.events({
    'submit .register-form': function(event) {
        event.preventDefault();
        var $codename = $('.register-codename'),
            $register_age = $('.register-age'),
            $gender = $('input[name="register-gender"]:checked');
        var pathname = location.pathname;
        var groupID = pathname.replace('/register/', '');
        ManitoManita.insert({
            codename: $codename.val(),
            age: $register_age.val(),
            gender: $gender.val(),
            user_id: Meteor.user()._id,
            group_id: groupID
        });

        window.location = '/dashboard/'+groupID;
    },
    'click .register-form-cancel': function(event) {
        event.preventDefault();
        window.location = '/';
    }
});

Template.manitomanita.helpers({
	manitomanitas: function() {
		var pathname = location.pathname;
		var id = pathname.replace('/dashboard/', '');
		console.log(id);
		return ManitoManita.find({group_id: id});
	}
});

Template.wishlist.helpers({
	wishlist: function() {
		var pathname = location.pathname;
		var id = pathname.replace('/wishlist/', '');
		return WishList.find({manitomanita_id: id});
	}
});

Template.wishlist.events({
	'click .remove': function(event){
		event.preventDefault();
		WishList.remove(this._id);
	},
	'click .add': function(event) {
		event.preventDefault();
		var pathname = location.pathname;
		var id = pathname.replace('/wishlist/', '');
		WishList.insert({
			manitomanita_id : id,
    		title: $("#new-wish").val(),
    		description: $("#description").val()
		});
	}
});