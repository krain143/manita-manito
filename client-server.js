Groups = new Mongo.Collection("groups");
ManitoManita = new Mongo.Collection("manitomanita");
WishList = new Mongo.Collection("wishlist");
GenericItems = new Mongo.Collection("genericitems");


Router.route('/', function() {
    this.render('groupList');
});

Router.route('/wishlist/:_id', function() {
	this.render('wishlist');
});

Router.route('/dashboard/:_id', function() {
	this.render('manitomanita');
});

Router.route('register/:_id', function() {
    this.render('register_form');
});